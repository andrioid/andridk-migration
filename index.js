const fs = require("fs");
const csv = require("fast-csv");
const TurndownService = require("turndown");
const ts = new TurndownService();

var stream = fs.createReadStream("../andridk/andrioid-export.csv");

var csvStream = csv({ headers: true })
  .on("data", function(data) {
    const body = ts.turndown(data.body_value);
    const date = new Date(data.created * 1000);
    const tags = data.tags ? data.tags.split('|') : []
    let tagString = '['
    tags.forEach(t => {
      if (t != 'NULL') {
        tagString += `"${t}"`
      }
      
    })
    tagString += ']'
    const output = `
    ---
    date: "${date.toISOString()}"
    title: "${data.title}"
    body: "${body}"
    tags: ${tagString}
    `
    console.log(output)
    // console.log(data);
  })
  .on("end", function() {
    console.log("done");
  });

stream.pipe(csvStream);
